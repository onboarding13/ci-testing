import { getByTestId, render, screen, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import { HomePage } from "./HomePage";

describe("HomePage test", () => {
  it("should toggle text visibility on click on 'Show the hidden text' button", () => {
    render(<HomePage />);
    const toggleTextButton = screen.getByTestId("toggle-text");
    fireEvent.click(toggleTextButton);
    const hiddenText = screen.getByTestId("hidden-text");
    expect(hiddenText).toBeInTheDocument();
    fireEvent.click(toggleTextButton);
    expect(hiddenText).not.toBeInTheDocument();
  });

  it("should clear the input on click on the 'Clear input button'", () => {
    render(<HomePage />);
    const clearInputButton = screen.getByTestId("clear-input");
    const input = screen.getByTestId("input") as HTMLInputElement;
    fireEvent.change(input, { target: { value: "Hello!" } });
    expect(input.value).toBe("Hello!");
    fireEvent.click(clearInputButton);
    expect(input.value).toBe("");
  });
});
