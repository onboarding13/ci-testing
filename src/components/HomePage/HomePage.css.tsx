import styled from "@emotion/styled";

export const ContentWrapper = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
`;

export const Header = styled.div`
  width: 100%;
  margin-bottom: 100px;
  text-align: center;
  font-size: 30px;
`;

export const ActionButton = styled.button`
  width: 300px;
  height: 100px;
  margin-bottom: 100px;
  font-size: 20px;
  background-color: white;
  border: 1px solid black;
  cursor: pointer;
  &:hover {
    box-shadow: 0 0 2px 2px black;
  }
`;

export const TextToShow = styled.p``;

export const Input = styled.input`
  box-sizing: border-box;
  width: 300px;
  height: 50px;
  padding: 5px;
  border-radius: 10px;
`;
