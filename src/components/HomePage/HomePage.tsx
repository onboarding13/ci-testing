import * as React from "react";
import { useState } from "react";
import {
  ActionButton,
  ContentWrapper,
  Header,
  Input,
  TextToShow,
} from "./HomePage.css";

export const HomePage = (): JSX.Element => {
  const [isHidden, setIsHidden] = useState(true);
  const [inputValue, setInputValue] = useState("");

  const toggleVisibility = (): void => {
    setIsHidden(!isHidden);
  };

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
    const value = e.target.value;
    setInputValue(value);
  };

  const handleInputClear = (): void => {
    setInputValue("");
  };

  return (
    <ContentWrapper>
      <Header>Click the button!</Header>
      <ActionButton onClick={toggleVisibility} data-testid="toggle-text">
        {isHidden ? "Show the hidden text" : "Hide text"}
      </ActionButton>
      {isHidden ? (
        ""
      ) : (
        <TextToShow data-testid="hidden-text">This text was hidden! But Now you see it.</TextToShow>
      )}
      <ActionButton onClick={handleInputClear} data-testid="clear-input">
        Clear Input
      </ActionButton>
      <Input
        type="text"
        value={inputValue}
        onChange={handleInputChange}
        data-testid="input"
      ></Input>
    </ContentWrapper>
  );
};
