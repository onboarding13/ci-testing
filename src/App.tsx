import { Global } from "@emotion/react";
import * as React from "react";
import { HomePage } from "./components/HomePage/HomePage";
import { reset, global } from "./theme";

export const App = (): JSX.Element => (
  <>
    <Global styles={[global, reset]}></Global>
    <HomePage />
  </>
);
